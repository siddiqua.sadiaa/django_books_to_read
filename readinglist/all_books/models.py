from django.core.validators import MaxValueValidator
from django.db import models

# Create your models here.

class Book(models.Model):
    title           = models.TextField()
    category        = models.CharField(max_length=30)
    writer          = models.CharField(max_length=100)
    shelved_times   = models.IntegerField()
    avg_rating      = models.DecimalField(max_digits=5, decimal_places=2)
    total_ratings   = models.IntegerField()
    Publication_year= models.IntegerField(validators=[MaxValueValidator(2100)])
